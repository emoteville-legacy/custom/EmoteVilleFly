![](https://assets.emo.gg/logo/emoteville-gold3-small.png)
<br>
[Website](https://go.emo.gg/) - [Discord](https://go.emo.gg/discord) - [Issue Tracker](https://go.emo.gg/issue-tracker)

<br>
EmoteVilleFly is a spigot plugin that allows the player to fly at different speeds using energy, this is upgradable by creating different groups in the config.

The original documentation for this project is located [HERE](https://www.evernote.com/shard/s627/sh/20297abf-d9de-4fa9-92e9-e9ede9354907/4f8216d442b3b5bce99801ebe276aa43).

It is possible to interact with other plugins using the commands and placeholders as an input/output system, this is how most of our plugins work.


**_Commands_**

| Command | Description |
| ------ | ------ |
| /fly  | Enable/Disable your Fisher status. |
| /fly (player) | Force a player to enable/disable their status. |

**_Permissions_**

| Command | Description |
| ------ | ------ |
| emoteville.fly.(group) | Applies selected group from from config, if you enable/disable the feature then it re-checks the player group. |

**_Placeholders_**

| Placeholder | Description |
| ------ | ------ |
| %fly_status% | Returns ON/OFF depending on if you have it toggled.|
| %fly_energy% | Returns players current energy. |
| %fly_energy_max% | Returns max amount of energy for player. |
| %fly_regen% | Returns regen percent out of 100. |
| %fly_regenbar_100% | Return 100 ":" grey which change to green to highlight your prevent progess to full regen. (https://i.imgur.com/UqT2wmC.png). |
| %fly_regenbar_50% | Return 50 ":" grey which change to green to highlight your prevent progess to full regen.(https://i.imgur.com/UqT2wmC.png). |
| %fly_regenbar_25% | Return 25 ":" grey which change to green to highlight your prevent progess to full regen.(https://i.imgur.com/UqT2wmC.png). |
