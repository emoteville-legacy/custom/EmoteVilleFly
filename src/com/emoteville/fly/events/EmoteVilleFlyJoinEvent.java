package com.emoteville.fly.events;

import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.emoteville.fly.EmoteVilleFlyMain;
import com.emoteville.fly.objects.FlyInfos;

public class EmoteVilleFlyJoinEvent implements Listener
{
	@EventHandler
	public void onPlayerJoinEvent(PlayerJoinEvent e)
	{
		final Player player = e.getPlayer();
		final String playerName = player.getName();
		Map<String, FlyInfos> playersFlyInfos = EmoteVilleFlyMain.getPlugin().getCommandsHandler().getPlayersFlyInfos();
		
		if(playersFlyInfos.containsKey(playerName)
			&& playersFlyInfos.get(playerName).isFlyActive())
		{
			player.setAllowFlight(true);
			player.setFlying(true);
			player.setFlySpeed(playersFlyInfos.get(playerName).getFlySpeed());
		}
	}
}
