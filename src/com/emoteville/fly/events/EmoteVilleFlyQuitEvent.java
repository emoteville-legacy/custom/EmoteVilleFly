package com.emoteville.fly.events;

import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.emoteville.fly.EmoteVilleFlyMain;
import com.emoteville.fly.objects.FlyInfos;

public class EmoteVilleFlyQuitEvent implements Listener
{
	@EventHandler
	public void onPlayerQuitEvent(PlayerQuitEvent e)
	{
		final Player player = e.getPlayer();
		final String playerName = player.getName();
		Map<String, FlyInfos> playersFlyInfos = EmoteVilleFlyMain.getPlugin().getCommandsHandler().getPlayersFlyInfos();
		
		if(playersFlyInfos.containsKey(playerName)
			&& playersFlyInfos.get(playerName).isFlyActive()
			&& player.isFlying())
		{
			player.setAllowFlight(false);
			player.setFlying(false);
			player.setFlySpeed(0.1f);
		}
	}
}
