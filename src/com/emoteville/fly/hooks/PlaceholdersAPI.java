package com.emoteville.fly.hooks;

import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.entity.Player;

import com.emoteville.fly.EmoteVilleFlyMain;
import com.emoteville.fly.objects.FlyInfos;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import net.md_5.bungee.api.ChatColor;

public class PlaceholdersAPI extends PlaceholderExpansion {

    private EmoteVilleFlyMain plugin;
    
    private final Map<String, Object> configFile;
    
    public PlaceholdersAPI(EmoteVilleFlyMain plugin) throws FileNotFoundException
    { 
        this.plugin = plugin;
        configFile = EmoteVilleFlyMain.getJSONManager().getSimpleJSON("config");
    }

    /**
     * Because this is an internal class,
     * you must override this method to let PlaceholderAPI know to not unregister your expansion class when
     * PlaceholderAPI is reloaded
     *
     * @return true to persist through reloads
     */
    @Override
    public boolean persist()
    {
        return true;
    }

    /**
     * Because this is a internal class, this check is not needed
     * and we can simply return {@code true}
     *
     * @return Always true since it's an internal class.
     */
    @Override
    public boolean canRegister()
    {
        return true;
    }

    /**
     * The name of the person who created this expansion should go here.
     * <br>For convienience do we return the author from the plugin.yml
     * 
     * @return The name of the author as a String.
     */
    @Override
    public String getAuthor()
    {
        return plugin.getDescription().getAuthors().toString();
    }

    /**
     * The placeholder identifier should go here.
     * <br>This is what tells PlaceholderAPI to call our onRequest 
     * method to obtain a value if a placeholder starts with our 
     * identifier.
     * <br>The identifier has to be lowercase and can't contain _ or %
     *
     * @return The identifier in {@code %<identifier>_<value>%} as String.
     */
    @Override
    public String getIdentifier()
    {
        return "emotevillefly";
    }

    /**
     * This is the version of the expansion.
     * <br>You don't have to use numbers, since it is set as a String.
     *
     * For convienience do we return the version from the plugin.yml
     *
     * @return The version as a String.
     */
    @Override
    public String getVersion()
    {
        return plugin.getDescription().getVersion();
    }

    /**
     * This is the method called when a placeholder with our identifier 
     * is found and needs a value.
     * <br>We specify the value identifier in this method.
     * <br>Since version 2.9.1 can you use OfflinePlayers in your requests.
     *
     * @param  player
     *         A {@link org.bukkit.Player Player}.
     * @param  identifier
     *         A String containing the identifier/value.
     *
     * @return possibly-null String of the requested identifier.
     */
    @SuppressWarnings("unchecked")
	@Override
    public String onPlaceholderRequest(Player player, String identifier)
    {
        if(player == null){
            return "";
        }

        final String playerName = player.getName();
        final Map<String, FlyInfos> playersFlyInfos = EmoteVilleFlyMain.getPlugin().getCommandsHandler().getPlayersFlyInfos();

        if(identifier.equals("status"))
        {
        	if(playersFlyInfos.containsKey(playerName)
        		&& playersFlyInfos.get(playerName).isFlyActive())
        	{
        		return "ON";
        	}
        	else
        	{
        		return "OFF";
        	}
        }
        else if(identifier.equals("is_flying"))
        {
        	if(player.isFlying())
        	{
        		return "true";
        	}
        	else
        	{
        		return "false";
        	}
        }
        else if(identifier.equals("duration_left"))
        {
        	if(playersFlyInfos.containsKey(playerName))
        	{
        		NumberFormat formatter = new DecimalFormat("#.#");
        		return formatter.format(playersFlyInfos.get(playerName).getLeftFlyDuration());
        	}
        	else if(configFile.containsKey("groups"))
        	{
        		for(Entry<String, Map<String, String>> entry : ((Map<String, Map<String, String>>) configFile.get("groups")).entrySet())
        		{
        			final String group = entry.getKey();
        			if(player.hasPermission("emoteville.fly." + group))
        			{
        				return entry.getValue().get("duration");
        			}
        		}
        	}
        	return "";
        }
        else if(identifier.equals("duration_max"))
        {
        	if(configFile.containsKey("groups"))
        	{
        		for(Entry<String, Map<String, String>> entry : ((Map<String, Map<String, String>>) configFile.get("groups")).entrySet())
        		{
        			final String group = entry.getKey();
        			if(player.hasPermission("emoteville.fly." + group))
        			{
        				return entry.getValue().get("duration");
        			}
        		}
        	}
        	return "";
        }
        else if(identifier.equals("group"))
        {
        	if(configFile.containsKey("groups"))
        	{
        		for(Entry<String, Map<String, String>> entry : ((Map<String, Map<String, String>>) configFile.get("groups")).entrySet())
        		{
        			if(player.hasPermission("emoteville.fly." + entry.getKey()))
        			{
        				return entry.getKey();
        			}
        		}
        	}
        	return "";
        }
        else if(identifier.equals("regen_percentage"))
        {
        	if(playersFlyInfos.containsKey(playerName))
        	{
        		return Double.toString(playersFlyInfos.get(playerName).getRegenBarPercentage());
        	}
        	else
        	{
        		return "100";
        	}
        }
        else if(identifier.equals("regen_bar"))
        {
        	StringBuilder percentageBar = new StringBuilder();
        	
        	if(playersFlyInfos.containsKey(playerName))
        	{
        		final double regenBarPecentage = playersFlyInfos.get(playerName).getRegenBarPercentage();
        		
        		for(int i = 0; i < 100; i++)
        		{
        			if(i < regenBarPecentage)
        			{
        				percentageBar.append(ChatColor.GREEN + "|");
        			}
        			else
        			{
        				percentageBar.append(ChatColor.GRAY + "|");
        			}
        		}
        		return percentageBar.toString();
        	}
        	else
        	{
        		for(int i = 0; i < 100; i++)
        		{
        			percentageBar.append(ChatColor.GREEN + "|");
        		}
        		return percentageBar.toString();
        	}
        }
        
        return null;
    }
}