package com.emoteville.fly.schedulers;

import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.emoteville.fly.EmoteVilleFlyMain;
import com.emoteville.fly.objects.FlyInfos;
import com.emoteville.fly.utils.EnumCheckUtils;

public class ParticlesSchedulers extends BukkitRunnable
{
	@SuppressWarnings("deprecation")
	@Override
	public void run()
	{
		Map<String, FlyInfos> playersFlyInfos = EmoteVilleFlyMain.getPlugin().getCommandsHandler().getPlayersFlyInfos();
		
		for(Entry<String, FlyInfos> entry : playersFlyInfos.entrySet())
		{
			final OfflinePlayer offPlayer = Bukkit.getOfflinePlayer(entry.getKey());
			if(offPlayer.isOnline()
				&& ((Player) offPlayer).isFlying()
				&& entry.getValue().getParticlesEffect() != null)
			{
				final Player player = (Player) offPlayer;
				final Location pLoc = player.getLocation();
				final Particle particleEffect = EnumCheckUtils.isValidEnum(Particle.class, entry.getValue().getParticlesEffect()) ? Particle.valueOf(entry.getValue().getParticlesEffect()) : Particle.CRIT;
				final int particleAmount = entry.getValue().getParticlesEffect() != null ? entry.getValue().getParticlesEffectAmount() : 5;
				
				player.getWorld().spawnParticle(particleEffect, pLoc.getX(), pLoc.getY() - 0.1, pLoc.getZ(), particleAmount, 0, 0, 0, 0, null, false);
			}
		}
	}
}
