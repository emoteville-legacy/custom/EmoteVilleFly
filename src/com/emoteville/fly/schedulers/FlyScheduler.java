package com.emoteville.fly.schedulers;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.emoteville.fly.EmoteVilleFlyMain;
import com.emoteville.fly.objects.FlyInfos;
import com.emoteville.fly.utils.ColorUtils;

public class FlyScheduler extends BukkitRunnable
{
	private final Map<String, Object> messagesFile;
	private final String prefix;
	
	public FlyScheduler() throws FileNotFoundException
	{
		messagesFile = EmoteVilleFlyMain.getJSONManager().getSimpleJSON("messages");
		prefix = (String) messagesFile.get("prefix");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void run()
	{
		Map<String, FlyInfos> playersFlyInfos = EmoteVilleFlyMain.getPlugin().getCommandsHandler().getPlayersFlyInfos();

		for(Player player : Bukkit.getServer().getOnlinePlayers())
		{
			final String playerName = player.getName();
			if(playersFlyInfos.containsKey(playerName))
			{
				final FlyInfos pFlyInfos = playersFlyInfos.get(playerName);
				if(pFlyInfos.isFlyActive()
					&& pFlyInfos.getLeftFlyDuration() > 0
					&& player.isFlying())
				{
					final double depletePerSecond = pFlyInfos.getMaxFlyDuration() / 100.0;
					final double newDurationLeft = (pFlyInfos.getLeftFlyDuration() - depletePerSecond) > 0.0 ? (pFlyInfos.getLeftFlyDuration() - depletePerSecond) : 0.0;
					if(pFlyInfos.getLeftFlyDuration() > pFlyInfos.getMaxFlyDuration()/10
						&& newDurationLeft <= pFlyInfos.getMaxFlyDuration()/10)
					{
						if(messagesFile.containsKey("warning-10-percent-stamina-left"))
						{
							ColorUtils.sendMessage(player, (List<String>) messagesFile.get("warning-10-percent-stamina-left"), prefix);
						}
					}
					pFlyInfos.setLeftFlyDuration(newDurationLeft)
							 .setRegenBarPercentage(Math.round((newDurationLeft / pFlyInfos.getMaxFlyDuration()) * 100));
				}
				else if(pFlyInfos.isFlyActive()
						&& pFlyInfos.getLeftFlyDuration() == 0
						&& player.isFlying())
				{
					playersFlyInfos.get(playerName).setFlyActive(false);
					player.setFlying(false);
					player.setAllowFlight(false);
					player.setFlySpeed(0.1f);
					if(!playersFlyInfos.get(playerName).hasFallDamage())
					{
						player.setFallDistance(-500);
					}
					if(messagesFile.containsKey("out-of-fly-stamina"))
					{
						ColorUtils.sendMessage(player, (List<String>) messagesFile.get("out-of-fly-stamina"), prefix);
					}
				}
				else if(!player.isFlying()
						&& pFlyInfos.getLeftFlyDuration() < pFlyInfos.getMaxFlyDuration())
				{
					double regenPerSecond = pFlyInfos.getMaxFlyDuration() / pFlyInfos.getMaxRegenTime();
					final double newDurationLeft = pFlyInfos.getLeftFlyDuration() + regenPerSecond;
					pFlyInfos.setLeftFlyDuration(newDurationLeft)
					 		 .setRegenBarPercentage(Math.round((newDurationLeft / pFlyInfos.getMaxFlyDuration()) * 100));
				}
			}
		}
	}
	
}
