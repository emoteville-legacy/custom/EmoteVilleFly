package com.emoteville.fly;

import java.io.FileNotFoundException;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import com.emoteville.fly.commands.EmoteVilleFlyCommandsHandler;
import com.emoteville.fly.events.EmoteVilleFlyJoinEvent;
import com.emoteville.fly.events.EmoteVilleFlyQuitEvent;
import com.emoteville.fly.hooks.PlaceholdersAPI;
import com.emoteville.fly.schedulers.FlyScheduler;
import com.emoteville.fly.schedulers.ParticlesSchedulers;
import com.emoteville.fly.utils.EnabledDependenciesUtils;
import com.emoteville.fly.utils.JSONManagerUtils;

public class EmoteVilleFlyMain extends JavaPlugin
{
	private static EmoteVilleFlyMain plugin;
	private static JSONManagerUtils jsonManager;
	private EmoteVilleFlyCommandsHandler commandsHandler;
	private BukkitTask flyScheduler;
	private BukkitTask particlesScheduler;

	@Override
	public void onEnable()
	{
		plugin = this;
		jsonManager = new JSONManagerUtils();
		jsonManager.createSimpleJSON("config");
		jsonManager.createSimpleJSON("messages");
		
		try
		{
			commandsHandler = new EmoteVilleFlyCommandsHandler();
			getCommand("emotevillefly").setExecutor(commandsHandler);
			
			if(EnabledDependenciesUtils.getEnabledDependencies().contains("PlaceholderAPI"))
			{
				new PlaceholdersAPI(this).register();
			}
			
			flyScheduler = new FlyScheduler().runTaskTimer(this, 0L, 20L);
			particlesScheduler = new ParticlesSchedulers().runTaskTimer(this, 0L, Long.valueOf((String) jsonManager.getSimpleJSON("config").get("particles-timing")));
			
			registerEvents(this, new EmoteVilleFlyJoinEvent(), new EmoteVilleFlyQuitEvent());
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onDisable()
	{
		flyScheduler.cancel();
		particlesScheduler.cancel();
		
		for(String playerName : commandsHandler.getPlayersFlyInfos().keySet())
		{
			OfflinePlayer removeFly = Bukkit.getOfflinePlayer(playerName);
			if(removeFly.isOnline())
			{
				((Player) removeFly).setFlying(false);
				((Player) removeFly).setAllowFlight(false);
				((Player) removeFly).setFlySpeed(0.1f);
			}
		}
		EmoteVilleFlyMain.plugin = null;
	}
	
	public static JSONManagerUtils getJSONManager()
	{
		return jsonManager;
	}
	
	public static EmoteVilleFlyMain getPlugin()
	{
		return plugin;
	}
	
	public EmoteVilleFlyCommandsHandler getCommandsHandler() {
		return commandsHandler;
	}
	
	private void registerEvents(EmoteVilleFlyMain plugin, Listener... listeners)
	{
		for(Listener listener : listeners)
		{
			Bukkit.getPluginManager().registerEvents(listener, plugin);
		}
	}
}