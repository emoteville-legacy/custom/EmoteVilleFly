package com.emoteville.fly.objects;

public class FlyInfos
{
	private double regenBarPercentage;
	private boolean isFlyActive;
	private double leftFlyDuration;
	private double maxFlyDuration;
	private double maxRegenTime;
	private float flySpeed;
	private String particlesEffect;
	private int particlesEffectAmount;
	private boolean fallDamage;
	
	public FlyInfos() {
		regenBarPercentage = 100;
		isFlyActive = true;
	}
	
	public double getRegenBarPercentage() {
		return regenBarPercentage;
	}
	
	public FlyInfos setRegenBarPercentage(double regenBar) {
		this.regenBarPercentage = regenBar;
		return this;
	}
	
	public boolean isFlyActive() {
		return isFlyActive;
	}
	
	public FlyInfos setFlyActive(boolean isFlyActive) {
		this.isFlyActive = isFlyActive;
		return this;
	}

	public double getLeftFlyDuration() {
		return leftFlyDuration;
	}

	public FlyInfos setLeftFlyDuration(double leftFlyDuration) {
		this.leftFlyDuration = leftFlyDuration;
		return this;
	}

	public double getMaxFlyDuration() {
		return maxFlyDuration;
	}

	public FlyInfos setMaxFlyDuration(double maxFlyDuration) {
		this.maxFlyDuration = maxFlyDuration;
		return this;
	}
	
	public double getMaxRegenTime() {
		return maxRegenTime;
	}

	public FlyInfos setMaxRegenTime(double regenTime) {
		this.maxRegenTime = regenTime;
		return this;
	}

	public String getParticlesEffect() {
		return particlesEffect;
	}

	public FlyInfos setParticlesEffect(String particlesEffect) {
		this.particlesEffect = particlesEffect;
		return this;
	}

	public int getParticlesEffectAmount() {
		return particlesEffectAmount;
	}

	public FlyInfos setParticlesEffectAmount(int particlesEffectAmount) {
		this.particlesEffectAmount = particlesEffectAmount;
		return this;
	}

	public float getFlySpeed() {
		return flySpeed;
	}

	public FlyInfos setFlySpeed(float flySpeed) {
		this.flySpeed = flySpeed;
		return this;
	}

	public boolean hasFallDamage() {
		return fallDamage;
	}

	public FlyInfos setFallDamage(boolean fallDamage) {
		this.fallDamage = fallDamage;
		return this;
	}
}
