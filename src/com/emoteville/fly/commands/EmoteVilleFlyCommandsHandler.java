package com.emoteville.fly.commands;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.emoteville.fly.EmoteVilleFlyMain;
import com.emoteville.fly.objects.FlyInfos;
import com.emoteville.fly.utils.ColorUtils;

public class EmoteVilleFlyCommandsHandler implements CommandExecutor
{
	
	private final Map<String, Object> configFile;
	private final Map<String, Object> messagesFile;
	private Map<String, FlyInfos> playersFlyInfos;

	private final String prefix;

	public EmoteVilleFlyCommandsHandler() throws FileNotFoundException
	{
		configFile = EmoteVilleFlyMain.getJSONManager().getSimpleJSON("config");
		messagesFile = EmoteVilleFlyMain.getJSONManager().getSimpleJSON("messages");
		playersFlyInfos = new HashMap<>();
		prefix = ColorUtils.color((String) messagesFile.get("prefix"));
	}
	
	public void setPlayersFlyInfos(Map<String, FlyInfos> playersFlyInfos) {
		this.playersFlyInfos = playersFlyInfos;
	}

	public Map<String, FlyInfos> getPlayersFlyInfos() {
		return playersFlyInfos;
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if(args.length == 0)
		{
			if(sender.hasPermission("emoteville.fly"))
			{
				if(sender instanceof Player)
				{
					final Player player = ((Player) sender).getPlayer();
					final String playerName = player.getName();
					
					for(Entry<String, Map<String, String>> entry : ((Map<String, Map<String, String>>) configFile.get("groups")).entrySet())
					{
						final String group = entry.getKey();
						
						if(player.hasPermission("emoteville.fly." + group))
						{
							if(!playersFlyInfos.containsKey(playerName)
									|| (playersFlyInfos.containsKey(playerName))
									&& !playersFlyInfos.get(playerName).isFlyActive())
							{
								if(!playersFlyInfos.containsKey(playerName))
								{
									final FlyInfos playerFlyInfos = new FlyInfos().setLeftFlyDuration(entry.getValue().containsKey("duration") ? Double.valueOf(entry.getValue().get("duration")) : 120)
											.setMaxFlyDuration(entry.getValue().containsKey("duration") ? Double.valueOf(entry.getValue().get("duration")) : 120)
											.setMaxRegenTime(entry.getValue().containsKey("regen") ? Double.valueOf(entry.getValue().get("regen")) : 600)
											.setFlySpeed(entry.getValue().containsKey("speed") && Float.valueOf(entry.getValue().get("speed")) > 10 ? 1 : entry.getValue().containsKey("speed") ? Float.valueOf(entry.getValue().get("speed"))/10 : 0.1f)
											.setParticlesEffect(entry.getValue().containsKey("particle") ? entry.getValue().get("particle") : null)
											.setParticlesEffectAmount(entry.getValue().containsKey("particle-amount") ? Integer.valueOf(entry.getValue().get("particle-amount")) : 5)
											.setFallDamage(entry.getValue().containsKey("fall-damage") ? Boolean.valueOf(entry.getValue().get("fall-damage")) : false);
									playersFlyInfos.put(playerName, playerFlyInfos);
								}
								
								playersFlyInfos.get(playerName).setFlyActive(true);
								player.setAllowFlight(true);
								player.setFlying(true);
								player.setFlySpeed(playersFlyInfos.get(playerName).getFlySpeed());
								if(messagesFile.containsKey("fly-enabled"))
								{
									ColorUtils.sendMessage(sender, (List<String>) messagesFile.get("fly-enabled"), prefix);
								}
							}
							else
							{
								playersFlyInfos.get(playerName).setFlyActive(false);
								player.setFlying(false);
								player.setAllowFlight(false);
								player.setFlySpeed(0.1f);
								if(messagesFile.containsKey("fly-disabled"))
								{
									ColorUtils.sendMessage(sender, (List<String>) messagesFile.get("fly-disabled"), prefix);
								}
							}
							return true;
						}
					}
					ColorUtils.sendMessage(sender, ((Map<String, List<String>>) messagesFile.get("errors")).get("not-enough-permission"), prefix);
				}
				else
				{
					ColorUtils.sendMessage(sender, ((Map<String, List<String>>) messagesFile.get("errors")).get("must-be-a-player"), prefix);
				}
			}
			else
			{
				ColorUtils.sendMessage(sender, ((Map<String, List<String>>) messagesFile.get("errors")).get("not-enough-permission"), prefix);
			}
		}
		else if(args.length == 1)
		{
			if(args[0].equalsIgnoreCase("help"))
			{
				if(messagesFile.containsKey("help-command"))
				{
					ColorUtils.sendMessage(sender, (List<String>) messagesFile.get("help-command"), prefix);
				}
				else
				{
					EmoteVilleFlyMain.getPlugin().getLogger().severe("An error occured while trying to parse the help command message. Please, check your messages file.");
				}
			}
			else if(args[0].equalsIgnoreCase("reload"))
			{
				if(sender.hasPermission("emoteville.fly.reload"))
				{
					if(messagesFile.containsKey("reload-command"))
					{
						ColorUtils.sendMessage(sender, (List<String>) messagesFile.get("reload-command"), prefix);
					}
					else
					{
						EmoteVilleFlyMain.getPlugin().getLogger().severe("An error occured while trying to parse the reload command message. Please, check your messages file.");
					}
					final EmoteVilleFlyMain mainInstance = EmoteVilleFlyMain.getPlugin();
					Bukkit.getPluginManager().disablePlugin(mainInstance);
					Bukkit.getPluginManager().enablePlugin(mainInstance);
				}
				else
				{
					ColorUtils.sendMessage(sender, ((Map<String, List<String>>) messagesFile.get("errors")).get("not-enough-permission"), prefix);
				}
			}
			else
			{
				if(sender.hasPermission("emoteville.fly.other"))
				{
					if(Bukkit.getOfflinePlayer(args[0]).isOnline())
					{
						final Player player = ((Player) Bukkit.getOfflinePlayer(args[0]));
						final String playerName = player.getName();
						
						for(Entry<String, Map<String, String>> entry : ((Map<String, Map<String, String>>) configFile.get("groups")).entrySet())
						{
							final String group = entry.getKey();
							
							if(player.hasPermission("emoteville.fly." + group))
							{
								if(!playersFlyInfos.containsKey(playerName)
										|| (playersFlyInfos.containsKey(playerName))
										&& !playersFlyInfos.get(playerName).isFlyActive())
								{
									if(!playersFlyInfos.containsKey(playerName))
									{
										final FlyInfos playerFlyInfos = new FlyInfos().setLeftFlyDuration(entry.getValue().containsKey("duration") ? Double.valueOf(entry.getValue().get("duration")) : 120)
												.setMaxFlyDuration(entry.getValue().containsKey("duration") ? Double.valueOf(entry.getValue().get("duration")) : 120)
												.setMaxRegenTime(entry.getValue().containsKey("regen") ? Double.valueOf(entry.getValue().get("regen")) : 600)
												.setFlySpeed(entry.getValue().containsKey("speed") && Float.valueOf(entry.getValue().get("speed")) > 10 ? 1 : entry.getValue().containsKey("speed") ? Float.valueOf(entry.getValue().get("speed"))/10 : 0.1f)
												.setParticlesEffect(entry.getValue().containsKey("particle") ? entry.getValue().get("particle") : null)
												.setParticlesEffectAmount(entry.getValue().containsKey("particle-amount") ? Integer.valueOf(entry.getValue().get("particle-amount")) : 5)
												.setFallDamage(entry.getValue().containsKey("fall-damage") ? Boolean.valueOf(entry.getValue().get("fall-damage")) : false);
										playersFlyInfos.put(playerName, playerFlyInfos);
									}
									
									playersFlyInfos.get(playerName).setFlyActive(true);
									player.setAllowFlight(true);
									player.setFlying(true);
									player.setFlySpeed(playersFlyInfos.get(playerName).getFlySpeed());
									if(messagesFile.containsKey("fly-enabled-other"))
									{
										for(String line : (List<String>) messagesFile.get("fly-enabled-other"))
										{
											ColorUtils.sendMessage(sender, line.replace("{player}", playerName), prefix);
										}
									}
								}
								else
								{
									playersFlyInfos.get(playerName).setFlyActive(false);
									player.setFlying(false);
									player.setAllowFlight(false);
									player.setFlySpeed(0.1f);
									if(messagesFile.containsKey("fly-disabled-other"))
									{
										for(String line : (List<String>) messagesFile.get("fly-disabled-other"))
										{
											ColorUtils.sendMessage(sender, line.replace("{player}", playerName), prefix);
										}
									}
								}
								break;
							}
						}
					}
				}
				else
				{
					ColorUtils.sendMessage(sender, ((Map<String, List<String>>) messagesFile.get("errors")).get("not-enough-permission"), prefix);
				}
			}
		}
		return false;
	}
	
}
