package com.emoteville.fly.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;

import com.emoteville.fly.EmoteVilleFlyMain;

public final class EnabledDependenciesUtils {
	
	private EnabledDependenciesUtils() {}
	
	public static List<String> getEnabledDependencies()
	{
		List<String> enabledDependencies = new ArrayList<>();
		
		for(String softdependencies : EmoteVilleFlyMain.getPlugin().getDescription().getSoftDepend())
		{
			if(Bukkit.getPluginManager().isPluginEnabled(softdependencies))
			{
				EmoteVilleFlyMain.getPlugin().getLogger().info(softdependencies + " has been found! Hook enabled!");
				enabledDependencies.add(softdependencies);
			}
			else
			{
				EmoteVilleFlyMain.getPlugin().getLogger().info(softdependencies + " not found! Hook disabled!");
			}
		}
		
		return enabledDependencies;
	}
}

