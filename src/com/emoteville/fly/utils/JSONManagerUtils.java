package com.emoteville.fly.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;

import com.emoteville.fly.EmoteVilleFlyMain;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class JSONManagerUtils
{
	private static EmoteVilleFlyMain plugin = EmoteVilleFlyMain.getPlugin();
	private Map<String, Map<String, Object>> simpleJSON;
	private final Gson gson = new GsonBuilder().setPrettyPrinting().create();
	
	public JSONManagerUtils()
	{
		simpleJSON = new HashMap<>();
	}
	
	/**
	 * Get any file loaded on the file HashMap by name
	 * 
	 * @fileName: Name of the configuration file to get	
	 */
    public Map<String, Object> getSimpleJSON(String fileName) throws FileNotFoundException
    {
    	if(this.simpleJSON.containsKey(fileName))
    	{
    		return this.simpleJSON.get(fileName);
    	}
    	else
    	{
    		Bukkit.getLogger().severe("The file " + fileName + " doens't exists or hasn't been loaded yet!");
    		Bukkit.getPluginManager().disablePlugin(plugin);
    		throw new FileNotFoundException();
    	}
    }
    
    /**
     * Simple way to make and save any config file to a HashMap to use them later
	 * It avoids copy/paste any kind of method to create any new custom config file
	 * 
	 * fileName: Name of the file to create as a config.
	 * 
	 * NB: Doesn't create it if it already exists and only load it in the HashMap
     */
	@SuppressWarnings("unchecked")
	public void createSimpleJSON(String fileName)
    {
		try
		{
    		File jsonFile = new File(plugin.getDataFolder(), fileName + ".json");    		
    		if (!jsonFile.exists())
    		{
    			jsonFile.getParentFile().mkdirs();
    			plugin.saveResource(fileName + ".json", false);
    		}
    		
    		final Map<String, Object> jsonMap = gson.fromJson(new FileReader(jsonFile), HashMap.class);
    		simpleJSON.put(fileName, jsonMap);
		}
		catch (JsonSyntaxException | JsonIOException | FileNotFoundException e)
		{
			plugin.getLogger().severe("An error occured while creating the " + fileName + " file!");
			e.printStackTrace();
		}
    }
	
    public void saveSimpleJSON(String fileName)
    {
    	File jsonFile = new File(plugin.getDataFolder(), fileName + ".json");
    	
    	try
    	{
    		final String json = gson.toJson(simpleJSON.get(fileName));
    		Files.delete(jsonFile.toPath());
    		Files.write(jsonFile.toPath(), json.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
		}
    	catch (IOException e)
    	{
    		plugin.getLogger().severe("An error occured while saving the " + fileName + " file!");
			e.printStackTrace();
		}
    }
}
